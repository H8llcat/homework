package com.h8llcat.pages;

import com.h8llcat.Browser;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

public class GenericPage {
    public static final ExpectedCondition<Boolean> EXPECT_DOC_READY_STATE = driver -> {
        String script = "if (typeof window != 'undefined' && window.document) { return window.document.readyState; } else { return 'notready'; }";
        Boolean result;
        try {
            assert driver != null;
            result = ((JavascriptExecutor) driver).executeScript(script).equals("complete");
        } catch (Exception ex) {
            result = Boolean.FALSE;
        }
        return result;
    };

    @SafeVarargs
    public static boolean waitForPageLoad(ExpectedCondition<Boolean>... conditions) {
        boolean isLoaded = false;

        Wait<WebDriver> wait = new FluentWait<>(Browser.getDriver())
                .withTimeout(120, TimeUnit.SECONDS)
                .ignoring(StaleElementReferenceException.class)
                .pollingEvery(1, TimeUnit.SECONDS);
        for (ExpectedCondition<Boolean> condition : conditions) {
            isLoaded = wait.until(condition);
            if (!isLoaded) {
                break;
            }
        }
        return isLoaded;
    }

    public void click(WebElement element) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) Browser.getDriver();
            js.executeScript("var evt = document.createEvent('MouseEvents');" + "evt.initMouseEvent('click',true, true, " +
                    "window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);" + "arguments[0].dispatchEvent(evt);", element);
        } catch (WebDriverException e) {
            throw new WebDriverException(e.toString());
        }
    }

    public void waitThenClick(WebElement element) {
        FluentWait wait = new FluentWait(Browser.getDriver())
                .withTimeout(120, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(StaleElementReferenceException.class, java.util.NoSuchElementException.class);
        try {
            waitForPageLoad(
                    EXPECT_DOC_READY_STATE
            );

            try {
                ((JavascriptExecutor) Browser.getDriver()).executeScript(
                        "arguments[0].scrollIntoView(true);", element);
            } catch (Exception ignored) {
            }

            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));

            JavascriptExecutor executor = (JavascriptExecutor) Browser.getDriver();
            executor.executeScript("arguments[0].click();", element);

            waitForPageLoad(
                    EXPECT_DOC_READY_STATE
            );
        } catch (WebDriverException e) {
            throw new WebDriverException(e.toString());
        }
    }

    public void waitThenClearAndSet(WebElement field, String newValue) {
        FluentWait wait = new FluentWait(Browser.getDriver())
                .withTimeout(60, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(StaleElementReferenceException.class);

        waitForPageLoad(
                EXPECT_DOC_READY_STATE
        );
        try {
            wait.until(ExpectedConditions.visibilityOf(field));
            field.clear();
            field.sendKeys(newValue);
            waitForPageLoad(
                    EXPECT_DOC_READY_STATE
            );
        }
        catch(WebDriverException e) {
            throw new WebDriverException(e.toString());
        }
    }
}
