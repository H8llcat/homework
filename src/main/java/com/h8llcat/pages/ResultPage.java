package com.h8llcat.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultPage extends GenericPage {
    @FindBy(xpath = "//div[contains(@class,'item-price')]")
    private WebElement priceResult;

    public WebElement getPriceResult() {
        return priceResult;
    }
}
