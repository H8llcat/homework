package com.h8llcat.pages;

import com.h8llcat.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DefaultPage extends GenericPage {
    private static final String SELECT_REGION = "//a[contains(@title,'Объявления') and contains(.,'Санкт-Петербург')]";

    public WebElement getRegionPageByName(String region) {
        return Browser.getDriver().findElement(By.xpath(String.format(SELECT_REGION, region)));
    }
}
