package com.h8llcat.pages;

import com.h8llcat.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchPage extends GenericPage {
    @FindBy(xpath = "//input[@id='search']")
    private WebElement searchInput;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement searchSumbit;

    @FindBy(xpath = "//span[contains(.,'только с фото')]/../input")
    private WebElement photoCheckbox;

    @FindBy(xpath = "//*[@class='nulus__header']")
    private WebElement noResult;

    @FindAll(@FindBy(xpath = "//div[contains(.,'Вчера') and contains(@class, 'date')]/../../../div/div[contains(@class,'about')]"))
    private List<WebElement> elements;


    public WebElement getSearchInput() {
        return searchInput;
    }

    public WebElement getSearchSumbit() {
        return searchSumbit;
    }

    public WebElement getPhotoCheckbox() {
        return photoCheckbox;
    }

    public WebElement getNoResult() {
        return noResult;
    }

    public WebElement getResult(int min, int max) {
        for (WebElement element : elements) {
            int price = Integer.valueOf(element.getText().replaceAll("[^0-9,]",""));
            if (min <=  price && price <= max) return element.findElement(By.xpath("../h3/a"));
        }
        return null;
    }
}
