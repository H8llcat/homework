package com.h8llcat.flows;

import com.h8llcat.Browser;
import com.h8llcat.pages.SearchPage;
import org.openqa.selenium.support.PageFactory;

public class SearchFlow extends GenericFlow {
    private static SearchPage page = PageFactory.initElements(Browser.getDriver(),
            SearchPage.class);

    public static void searchByName(String name) {
        page.waitThenClearAndSet(page.getSearchInput(), name);
    }

    public static void onlyWithPhoto(boolean set) {
        if ((set && !page.getPhotoCheckbox().isSelected()) || (!set && page.getPhotoCheckbox().isSelected()))
            page.click(page.getPhotoCheckbox());
    }

    public static void submitForm() {
        page.click(page.getSearchSumbit());
    }

    public static boolean isNoResult() {
        return page.getNoResult().isDisplayed();
    }

    public static void clickOnResult(int min, int max) {
        page.click(page.getResult(min, max));
    }
}
