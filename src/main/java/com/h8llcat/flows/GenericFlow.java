package com.h8llcat.flows;

import com.h8llcat.pages.GenericPage;

import static com.h8llcat.pages.GenericPage.waitForPageLoad;

public class GenericFlow {
    public static boolean isLoaded() {
        return waitForPageLoad(GenericPage.EXPECT_DOC_READY_STATE);
    }
}
