package com.h8llcat.flows;

import com.h8llcat.Browser;
import com.h8llcat.pages.DefaultPage;
import org.openqa.selenium.support.PageFactory;

public class DefaultFlow extends GenericFlow {
    private static DefaultPage page = PageFactory.initElements(Browser.getDriver(),
            DefaultPage.class);

    public static void selectRegion(String region) {
        page.waitThenClick(page.getRegionPageByName(region));
    }
}
