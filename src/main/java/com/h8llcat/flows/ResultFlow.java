package com.h8llcat.flows;

import com.h8llcat.Browser;
import com.h8llcat.pages.ResultPage;
import org.openqa.selenium.support.PageFactory;

public class ResultFlow extends DefaultFlow {
    private static ResultPage page = PageFactory.initElements(Browser.getDriver(),
            ResultPage.class);

    public static boolean isTrueResult(int min, int max) {
        int price = Integer.valueOf(page.getPriceResult().getText().replaceAll("[^0-9,]",""));
        return (min <= price && price <= max);
    }
}
