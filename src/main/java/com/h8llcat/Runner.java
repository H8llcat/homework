package com.h8llcat;

import com.h8llcat.flows.DefaultFlow;
import com.h8llcat.flows.ResultFlow;
import com.h8llcat.flows.SearchFlow;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Runner {
    private SoftAssert softAssertion = new SoftAssert();

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        Browser.getDriver();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        Browser.getDriver().quit();
    }

    @Test(groups = {"home"})
    public void checkPageLoad() {
        Browser.getDriver().get("https://www.avito.ru/");
        DefaultFlow.selectRegion("Санкт-Петербург");

        softAssertion.assertTrue(DefaultFlow.isLoaded());
        softAssertion.assertAll();
    }

    @Test(groups = {"home"}, dependsOnMethods = {"checkPageLoad"})
    public void negativeSearch() {
        SearchFlow.onlyWithPhoto(true);
        SearchFlow.searchByName("Рлвдыоа Hjsdfk 124456");
        SearchFlow.submitForm();

        softAssertion.assertTrue(SearchFlow.isNoResult());
        softAssertion.assertAll();
    }

    @Test(groups = {"home"}, dependsOnMethods = {"negativeSearch"})
    public void positiveSearch() {
        SearchFlow.onlyWithPhoto(false);
        SearchFlow.searchByName("MacBook pro");
        SearchFlow.submitForm();

        SearchFlow.clickOnResult(50000, 100000);

        softAssertion.assertTrue(ResultFlow.isTrueResult(50000, 100000));
        softAssertion.assertAll();
    }
}
